module example

go 1.15

require (
	github.com/julienschmidt/httprouter v1.3.0
	github.com/urfave/cli v1.22.4
	go.uber.org/zap v1.16.0
)
