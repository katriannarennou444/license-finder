#!/bin/bash

export ASDF_DATA_DIR="/opt/asdf"
export PATH="${ASDF_DATA_DIR}/shims:${ASDF_DATA_DIR}/bin:/opt/gitlab/.local/bin:${PATH}"
export HOME="/opt/gitlab"

set -o vi

function inflate() {
  local file=$1
  local to_dir=$2
  if [ -f "$file" ]; then
    tar --use-compress-program zstd -xf "$file" -C "$to_dir"
    rm "$file"
  fi
}

function enable_dev_mode() {
  unset HISTFILESIZE
  unset HISTSIZE
  export EDITOR=vim
  export LOG_LEVEL=debug
  set -o vi
  apt-get update -y
  apt-get install -y --no-install-recommends vim less shellcheck
}

if [ ! -d "/opt/asdf" ]; then
  dpkg -i /opt/toolcache/asdf_*.deb
fi
inflate /opt/gitlab/.config.tar.zst /opt/gitlab
inflate /opt/gitlab/.m2.tar.zst /opt/gitlab
inflate /opt/gitlab/embedded.tar.zst /opt/gitlab
inflate /usr/lib/git-core.tar.zst /usr/lib

# shellcheck source=/dev/null
. "$ASDF_DATA_DIR/asdf.sh"
# shellcheck source=/dev/null
. "$ASDF_DATA_DIR/completions/asdf.bash"
